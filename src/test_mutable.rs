use std::HashMap;

pub struct Boop<'a> {
    thing1: &'a i64,
    thing2: HashMap<'a,String,i64> 
}

impl<'a> Boop<'a> {
    pub fn new(thing2: HashMap<'a,String,i64>) -> Boop<'a> {
        let thing1 = &2;
        Boop {
            thing1,
            thing2
        }
    }

    pub fn addAVal(&self) -> (){
        self.thing2.insert("y",17)
    }
}