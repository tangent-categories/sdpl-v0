use inkwell::OptimizationLevel;
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::debug_info::LLVMDWARFTypeEncoding;
use inkwell::execution_engine::{ExecutionEngine, JitFunction};
use inkwell::module::Module;
use inkwell::targets::{InitializationConfig, Target,RelocMode,CodeModel,FileType,TargetMachine};
use inkwell::values::BasicValueEnum::{IntValue,FloatValue,StructValue};
use inkwell::values::{BasicValue,FunctionValue,GenericValue,BasicValueEnum,PointerValue};//needed for the code that allows using GlobalValues as BasicValueEnums
use inkwell::module::Linkage;
use inkwell::AddressSpace;
use inkwell::types::{BasicTypeEnum,FloatType,FunctionType,BasicType};
use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::error::Error;
use crate::ast::{TypedProgram,TypedFunDef,TypedTerm,TypedBTerm,Type,ConcreteType,ConcreteTypedTerm,ConcreteTypedFunDef,ConcreteTypedProgram};
use std::cell::{RefCell,Cell};//used to hold a mutable interior reference to the map of variable names
use std::rc::Rc;

pub fn make_llvm_stuff() -> Option<()> {
    //Define the context, a new module, and the builder (for adding items to blocks, etc)
    let context = Context::create();
    let module = context.create_module("sum");
    let builder = context.create_builder();
 

    //now let's add some basic types that we will use
    let i64_type = context.i64_type();
    let i32_type = context.i32_type();

    //in the future we should build a custom llvm file that links against llvm's libc for doing things like printing, malloc, free, etc
    //for now though, we'll juse bake the whole printf function with it's complicated type here.
    let str_type = context.i8_type().ptr_type(AddressSpace::Generic);
    let printf_type = i32_type.fn_type(&[BasicTypeEnum::PointerType(str_type)], true /* variadic */);

    //next, we'll declare the function printf to our module
    let printf = module.add_function("printf", printf_type, Some(Linkage::External));

    //next we'll define an llvm function to add two numbers and return the result.
    //generally the steps for doing this are: define the function's type, add the function to the module,
    //start a basic block, add instructions to it, and if the function has a return type, make the return statement.
    //later we'll go through each statement and say exactly what they do
    let fn_type = i64_type.fn_type(&[i64_type.into(),i64_type.into(),i64_type.into()],false);
    let function = module.add_function("sum",fn_type,None);
    let basic_block = context.append_basic_block(function,"entry");
    builder.position_at_end(basic_block);
    let x = function.get_nth_param(0)?.into_int_value();
    let y = function.get_nth_param(1)?.into_int_value();
    let z = function.get_nth_param(2)?.into_int_value();
    let sum = builder.build_int_add(x,y,"sum");
    let sum = builder.build_int_add(sum,z,"sum");
    builder.build_return(Some(&sum));

    //next we'll define the main function of the program
    let void_type=context.void_type();
    let main_fn_type = void_type.fn_type(&[],false);
    let main_fn = module.add_function("main",main_fn_type,Some(Linkage::External));
    let basic_block = context.append_basic_block(main_fn,"entry");
    builder.position_at_end(basic_block);
    //first let's call the function we wrote above to get the sum of 1,2,3
    let my_sum = builder.build_call(function,&[IntValue(i64_type.const_int(1,false)),IntValue(i64_type.const_int(2,false)),IntValue(i64_type.const_int(3,false))],"call").try_as_basic_value().left().unwrap();
    //now we make the format string for printf
    let format_string = builder.build_global_string_ptr("\n%d\n\n","fs");
    let print_sum = builder.build_call(printf,&[format_string.as_basic_value_enum(),my_sum],"call").try_as_basic_value().left().unwrap();
    builder.build_return(None);
    //main is a void function, so doesn't need a return perse, but it does require a basic block terminator, in this case "return void;"

    //that's it for the code gen.  The rest is just piping the writing to a file logic through.  We'll just emit the bitcode this time, 
    //and compile with llc
    let ir_output_filename = std::fs::File::create("output.bc").unwrap();
    module.write_bitcode_to_file(&ir_output_filename,true,true);

 
    return Some(());
}

// pub struct CompileContext<'ctx> {
//     pub context : Box<Context>,
//     pub builder: Builder<'ctx>,
//     pub double: FloatType<'ctx>,
//     pub variable_names:  RefCell<HashMap<String,(PointerValue<'ctx>,BasicTypeEnum<'ctx>)>>
// }

// //Then we can implement some functions that such a struct always has.
// impl<'ctx> CompileContext<'ctx> {
    // pub fn new(context: &'ctx Context) -> CompileContext<'ctx>{
    //     let double = context.f64_type();
    //     let builder = context.create_builder();
    //     let variable_names = RefCell::new(HashMap::new());
    //     CompileContext {
    //         context: context,
    //         builder,
    //         double,
    //         variable_names
    //     }
    // }
// pub type CompileContext<'ctx> = (Context,Builder<'ctx,FloatType,HashMap<String,(PointerValue,BasicTypeEnum)>);

    // pub fn get_variable_names(&self) -> &HashMap<String,BasicValueEnum> {
    //     &self.variable_names
    // }context:Contex,builder:&Builder,mut variable_names: &HashMap<String,(PointerValue,BasicTypeEnum)>
    // double:FloatType

    // pub fn new(context: <Context>) -> CompileContext<'ctx> {
    //     // let context = Context::create();
    //     let builder = context.create_builder();
    //     let double = context.f64_type();
    //     let variable_names = RefCell::new(HashMap::new());
    //     let ct = CompileContext {
    //         context,
    //         builder,
    //         double,
    //         variable_names
    //     };
    //     ct
    // }

pub fn compile_file_module<'a>(context: &'a Context,builder:&Builder<'a>,mut variable_names: &RefCell<HashMap<String,(PointerValue<'a>,BasicTypeEnum<'a>)>>,prog: & ConcreteTypedProgram,file_name: &str) -> (Module<'a>,Vec<FunctionValue<'a>>) {
    let mut module = context.create_module(file_name);
    let funcs = prog.iter().map(|x| compile_function(context,builder,variable_names,& mut module,x)).collect();
    (module,funcs)
}

    //since we only compile after type checking, everything in sight will have a type
pub fn compile_function<'a>(context: &'a Context,builder:&Builder<'a>,mut variable_names: &RefCell<HashMap<String,(PointerValue<'a>,BasicTypeEnum<'a>)>>,module: &mut Module<'a>,function:& ConcreteTypedFunDef) -> FunctionValue<'a> {
    match function {
        ConcreteTypedFunDef::ConcreteTypedFunDef(name,args,ret_ty,term) => {
            let fn_type = make_function_type(context,args,ret_ty);
            let function = module.add_function(name,fn_type,None);
            let basic_block = context.append_basic_block(function,"entry");
            builder.position_at_end(basic_block);
            //add all the variables to the context 
            let fn_params = function.get_params();//.iter().map(|llvm_arg| llvm_arg.into_pointer_value()).collect();
            // add our parameter names to variable_names.  We also allocate the variables and store the function parameters so we can 
            // call the variable names later without adjustment.  Since %n is not a variable name in our language there is no harm in this.
            // This should be optimized by the mem2reg pass.
            for (i,(var_name,var_ty)) in args.iter().enumerate() {
                //get the type of the ith param
                let ith_param_type = fn_params[i].get_type();
                //allocate
                let arg_i_allocated = builder.build_alloca(ith_param_type,var_name);
                //store
                builder.build_store(arg_i_allocated, fn_params[i]);
                //register the variable name in variable_names
                variable_names.borrow_mut().insert(var_name.clone(), (arg_i_allocated,ith_param_type));
            }
            //at this point the function in llvm should look like
            /*
            define TYPE @name(TYPE0 %0, TYPE1 %1,...,TYPEN %N) {
                entry:
                    %x0 = alloca(TYPE0,"x0")
                    store %0, %xo
                    ...
                    %xn = alloca(TYPEN,"xn")
                    store %n, %xn
                [|term|]
            }
            where [|m|] denotes the compilation of the body of the function, term.
            */
            //module is already mutable, so don't need to send it in as mutable
            let ret_val = compile_term(context,builder,variable_names,args, module,term);
            builder.build_return(Some(&ret_val));
            //TODO: we need to remove the variable names from the map.
            function
        }
    }
}

    //make_function_type creates the llvm function type reference 
pub fn make_function_type<'a>(context: &'a Context,args:&Vec<(String,Box<ConcreteType>)>,ret_ty:&Box<ConcreteType>) -> FunctionType<'a>{
    //compile the function's return type
    let fn_ret_ty = compile_type(context,ret_ty);
    //get the input type string
    let arg_types:Vec<BasicTypeEnum> = args.iter().map(|name_type| compile_type(context,&name_type.1)).collect();
    
    //return the llvm function type 
    fn_ret_ty.fn_type(&arg_types, false)
}

    

    pub fn compile_type<'a>(context: &'a Context,ty:&Box<ConcreteType>) -> BasicTypeEnum<'a> {
        match &**ty{
            ConcreteType::CR => BasicTypeEnum::FloatType(context.f64_type()),
            ConcreteType::CUnitType => BasicTypeEnum::StructType(context.struct_type(&[], false)),
            ConcreteType::CPairType(lty,rty) => BasicTypeEnum::StructType(context.struct_type(&[compile_type(context,&lty),compile_type(context,&rty)], false)),
        }
    }

    //since we store the args in the variable_names map, and we carry around said map, do we need to pass args around like this, or can we drop args from our list of inputs?
    pub fn compile_term<'a>(context: &'a Context,builder:&Builder<'a>,variable_names:&RefCell<HashMap<String,(PointerValue<'a>,BasicTypeEnum<'a>)>>,args: &Vec<(String,Box<ConcreteType>)>,module :&mut Module,term: &Box<ConcreteTypedTerm>) -> BasicValueEnum<'a> {
        match &**term{
            ConcreteTypedTerm::TNumber(n) => FloatValue(context.f64_type().const_float(*n)),
            ConcreteTypedTerm::TVar(x, x_ty) => {
                //get the llvm type of the variable
                // let x_ty_llvm = self.compile_type(&x_ty);
                //now the variable has been stored in module or context, so by type checking the following will never panic.
                //so we look up the variable in the map.  We also stored the variable's type with it, so we get that for free.
                //by type checking we know this matches the type we're of x.
                let (x_ref,x_ref_type) = variable_names.borrow_mut().get(x).unwrap().clone();//this is fine since by type checking, we know the variable is in the map already.
                //load the variable
                let loaded = builder.build_load(x_ref, x);
                loaded
            },
            ConcreteTypedTerm::TFunCall(_, _, _) => todo!(),
            ConcreteTypedTerm::TLet(_, _, _, _, _) => todo!(),
            ConcreteTypedTerm::TZero(_) => todo!(),
            ConcreteTypedTerm::TUnit => todo!(),
            ConcreteTypedTerm::TPlus(m, n, ty) => {
                match **ty {
                    ConcreteType::CR => {
                        let comp_m = compile_term(context, builder, variable_names, args, module, m).into_float_value();//the call to into_float_value() will succeed by type checking
                        let comp_n = compile_term(context, builder, variable_names, args, module, n).into_float_value();
                        let sum = builder.build_float_add(comp_m, comp_n, "sum");
                        FloatValue(sum)
                    },
                    ConcreteType::CUnitType => {
                        let unit_struct = context.const_struct(&[], true);
                        StructValue(unit_struct)
                    },
                    ConcreteType::CPairType(_, _) => make_addition_recursive(context,builder,variable_names,args,module,m,n,ty),
                }
            },
            ConcreteTypedTerm::TPair(_, _, _) => todo!(),
            ConcreteTypedTerm::TFst(_, _) => todo!(),
            ConcreteTypedTerm::TSnd(_, _) => todo!(),
            ConcreteTypedTerm::TWhen(_, _) => todo!(),
            ConcreteTypedTerm::TWhile(_, _, _, _) => todo!(),
            ConcreteTypedTerm::TDiff(_, _, _, _, _) => todo!(),
        }
    }

    //possibly get rid of args ... I don't think this is needed anymore.
    //note this function monotonically decreases ty and hence terminates
    fn make_addition_recursive<'a>(context: &'a Context, builder: &Builder<'a>, variable_names: &RefCell<HashMap<String, (PointerValue<'a>, BasicTypeEnum<'a>)>>,args: &Vec<(String,Box<ConcreteType>)>, module: &mut Module, m: &Box<ConcreteTypedTerm>, n: &Box<ConcreteTypedTerm>, ty: &Box<ConcreteType>) -> BasicValueEnum<'a> {
        match &**ty{
            ConcreteType::CR => {
                let comp_m = compile_term(context, builder, variable_names, args, module, m).into_float_value();//the call to into_float_value() will succeed by type checking
                let comp_n = compile_term(context, builder, variable_names, args, module, n).into_float_value();
                let sum = builder.build_float_add(comp_m, comp_n, "sum");
                FloatValue(sum)
            },
            ConcreteType::CUnitType => {
                let unit_struct = context.const_struct(&[], true);
                StructValue(unit_struct)
            },
            /*
            If we are adding two things that are a pair (a,b) then each of m and n is of type (a,b).
            But that means, that upon compiling, they are represented as {a,b}, and hence we can get element ptr for each of the two items.
             */
            ConcreteType::CPairType(lty, rty) => {
                let m0 = ConcreteTypedTerm::TFst(m.clone(),lty.clone());
                let m1 = ConcreteTypedTerm::TSnd(n.clone(),rty.clone());
                let n0 = ConcreteTypedTerm::TFst(n.clone(),lty.clone());
                let n1 = ConcreteTypedTerm::TSnd(n.clone(),rty.clone());
                let m0plusn0 = make_addition_recursive(context, builder, variable_names, args, module, &Box::new(m0), &Box::new(n0), &lty.clone());
                let m1plusn1 = make_addition_recursive(context, builder, variable_names, args, module, &Box::new(m1), &Box::new(n1), &rty.clone());
                //so far, we've descended to add on the base type.  Now we have to build the struct holding (m0plusn0,m1plusn1).
                //First we allocate the struct
                let sum_struct_type = compile_type(context, ty);
                let sum_ptr = builder.build_alloca(sum_struct_type, "sum_ptr");
                //next we set the first element of sum_ptr to be m0plusn0
                let sum_0_ptr = builder.build_struct_gep(sum_ptr, 0, "sum_0_ptr").unwrap();//we just created this pointer, so this succeeds as build_struct_gep fails if the ptr isn't to a struct
                builder.build_store(sum_0_ptr, m0plusn0);
                //similarly we set the second selement of sum_ptr to be m1plusn1
                let sum_1_ptr = builder.build_struct_gep(sum_ptr, 1, "sum_1_ptr").unwrap();
                builder.build_store(sum_1_ptr, m1plusn1);
                //now we load the struct, and then we finally return it
                let sum_struct = builder.build_load(sum_ptr, "sum_struct");
                sum_struct
            },
        }
    }

// }