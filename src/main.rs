#[macro_use] extern crate lalrpop_util;

lalrpop_mod!(pub language);
pub mod ast;
pub mod codegen;

use codegen::{make_llvm_stuff,compile_file_module};

use ast::egConcTypedProg;

//The goal of this step is still not to produce a working programming language.
//For this we need the semantics of a non-differential fragment.  Alternatively,
//We need to define a JIT-based machine implementation of the language.  
//Preferably we do both.  The JIT-based implementation allows testing and debugging 
//of ideas and then it's easy to lower this to a hard-compiled code that can run 
//say in a docker image or in a cloud function...
//The above will be sdpl-rust-v0.5
//This version here will be sdpl-rust-v0 and will simply provide an llvm library 
//that one can compile e.g. a C file against with clang.
use inkwell::OptimizationLevel;
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::execution_engine::{ExecutionEngine, JitFunction};
use inkwell::module::Module;
use inkwell::targets::{InitializationConfig, Target,RelocMode,CodeModel,FileType,TargetMachine};
use inkwell::values::BasicValueEnum::IntValue;
use inkwell::values::BasicValue;//needed for the code that allows using GlobalValues as BasicValueEnums
use inkwell::module::Linkage;
use inkwell::AddressSpace;
use inkwell::types::BasicTypeEnum;
use core::borrow;
use std::error::Error;
use std::collections::HashMap;
use std::cell::{RefCell,Cell};
use std::hash::Hash;

/*
After running this program, a bitcode file is produced.  We could have directly produced assembly, but we still have to link it anyways, so it still requires 
calling an external program.

At any rate, run this program, and you will create a file called output.bc
Then run 
    llc output.bc
    gcc -no-pie output.s -o output-exe
To produce the binary output-exe.  Note the -no-pie switch can be removed when we bundle in libc (or what we need from it) with the compiler.
Then run with 
    ./output-exe
And you should see the number 6 printed to the command line.
To view the llvm-ir code produced run
    llvm-dis output.bc
This will generate a file called output.ll.  The contents of this file contain the generated llvm-ir.
*/
fn main () {
    
    println!("hi");
    match make_llvm_stuff(){
        None => println!("failed to produce bitcode"),
        Some(()) => println!("bitcode produced successfully")
    }

    
    let egProg = &egConcTypedProg();
    let context = Context::create();
    // let context = &context_raw;
    
    let builder = context.create_builder();
    let variable_names = RefCell::new(HashMap::new());
    // let variable_names = HashMap::new();
    // let compile_context = CompileContext{
    //     context,
    //     builder,
    //     double,
    //     variable_names
    // };
    let (module,funcs) = compile_file_module(&context,&builder,&variable_names,egProg, "test");
    let ir_output_filename = std::fs::File::create("test.bc").unwrap();
    module.write_bitcode_to_file(&ir_output_filename,true,true);
    

    // let borrowed_cont = &llvm_context;
    // {
    //     let compile_context = CompileContext::new(borrowed_cont);
    //     {
    //         let zzzzz = compile_context.compile_file_module(egProg, "testEg");
    //     }
    // }
    

    // let (module,funcs) = CompileContext::new(&llvm_context).compile_file_module(&egConcTypedProg(), "testEgCompile");
    // let (module,funcs) = compile_context.compile_file_module(&egConcTypedProg(), "testEgCompile").clone();

}



#[test]
fn parser_tests(){
    assert!(language::TermParser::new().parse("22").is_ok());
    assert!(language::TermParser::new().parse("((22))").is_ok());
    assert!(language::TermParser::new().parse("((22)").is_err());
    assert!(language::TermParser::new().parse("22.1").is_ok());
    assert!(language::TermParser::new().parse("22.19e10").is_ok());
    assert!(language::TermParser::new().parse("22E10").is_ok());
    assert!(language::TermParser::new().parse("22.1E9").is_ok());
    assert!(language::FunDefParser::new().parse("f(x) = x").is_ok());
    assert!(language::FunDefParser::new().parse("f(x:R,y) = x").is_ok());
    assert!(language::FunDefParser::new().parse("f(x:R,y:R) = x").is_ok());
    assert!(language::FunDefParser::new().parse("f(x:R,y) -> R = x").is_ok());
    assert!(language::FunDefParser::new().parse("f(x,y) -> R = x").is_ok());
    assert!(language::FunDefParser::new().parse("f(x:R,y) -> = x").is_err());
    assert!(language::FunDefParser::new().parse("f(x,y:R) = x").is_ok());
    assert!(language::FunDefParser::new().parse("f(x,y:R) -> R = x").is_ok());
}