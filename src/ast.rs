pub type Program = Vec<FunDef>;

pub type TypedProgram = Vec<TypedFunDef>;

pub type ConcreteTypedProgram = Vec<ConcreteTypedFunDef>;

#[derive(Clone, Debug, PartialEq)]
pub enum FunDef {
    FunDef(String,Vec<(String,Option<Box<Type>>)>,Option<Box<Type>>,Box<Term>)
}

#[derive(Clone, Debug, PartialEq)]
pub enum TypedFunDef{
    TypedFunDef(String,Vec<(String,Box<Type>)>,Box<Type>,Box<TypedTerm>)
}

pub enum ConcreteTypedFunDef {
    ConcreteTypedFunDef(String,Vec<(String,Box<ConcreteType>)>,Box<ConcreteType>,Box<ConcreteTypedTerm>)
}

#[derive(Clone, Debug, PartialEq)]
pub enum Term {
    Number(f64),
    Var(String),
    FunCall(String,Vec<Box<Term>>),
    Let(String,Box<Term>,Box<Term>),
    Zero,
    Plus(Box<Term>,Box<Term>),
    Unit,
    Pair(Box<Term>,Box<Term>),
    Fst(Box<Term>),
    Snd(Box<Term>),
    When(Vec<(Box<BTermI<Term>>,Box<Term>)>),
    While(Vec<String>,Box<BTermI<Term>>,Box<Term>),
    Diff(Vec<String>,Box<Term>,Vec<Box<Term>>,Vec<Box<Term>>)
}

//We assume that type checking/inference generates well typed terms 
//storing the types at every node in the ast makes code gen easier, and maybe faster
//for a linear tradeoff in space
#[derive(Clone,Debug,PartialEq)]
pub enum TypedTermI<Typ> { 
    TNumber(f64),
    TVar(String,Box<Typ>), //x:A
    TFunCall(String,Box<Typ>,Vec<Box<TypedTermI<Typ>>>),//f:A(args)
    TLet(String,Box<Typ>,Box<TypedTermI<Typ>>,Box<TypedTermI<Typ>>,Box<Typ>),//x:A = m in n:B
    TZero(Box<Typ>),//0:A
    TUnit,
    TPlus(Box<TypedTermI<Typ>>,Box<TypedTermI<Typ>>,Box<Typ>),//m+n:A
    TPair(Box<TypedTermI<Typ>>,Box<TypedTermI<Typ>>,Box<Typ>),//(m,n):X -- by type checking X===(Y,Z)
    TFst(Box<TypedTermI<Typ>>,Box<Typ>),//TFst(m):A -- where by type checking m:(A,B)
    TSnd(Box<TypedTermI<Typ>>,Box<Typ>),//TSnd(m):B -- where by type checking m:(A,B)
    TWhen(Vec<(Box<BTermI<TypedTermI<Typ>>>,Box<TypedTermI<Typ>>)>,Box<Typ>),// when {bi -> mi} :A (where each mi:A by type checking)
    TWhile(Vec<(String,Box<Typ>)>,Box<BTermI<TypedTermI<Typ>>>,Box<TypedTermI<Typ>>,Box<Typ>),//on xs while b m : A where by type checking m:A
    TDiff(Vec<(String,Box<Typ>)>,Box<TypedTermI<Typ>>,Vec<Box<TypedTermI<Typ>>>,Vec<Box<TypedTermI<Typ>>>,Box<Typ>)//d(xs.m)(as).(vs):B where by type checking m:B
}

pub type TypedTerm = TypedTermI<Type>;
pub type ConcreteTypedTerm = TypedTermI<ConcreteType>;

pub fn get_type(term: TypedTerm) -> Type {
    match term {
        TypedTerm::TNumber(_x) => Type::R,
        TypedTerm::TVar(_, ty) => *ty,
        TypedTerm::TFunCall(_, ty, _) => *ty,
        TypedTerm::TLet(_, _, _, _, ty) => *ty,
        TypedTerm::TZero(ty) => *ty,
        TypedTerm::TUnit => Type::UnitType,
        TypedTerm::TPlus(_, _, ty) => *ty,
        TypedTerm::TPair(_, _, ty) => *ty,
        TypedTerm::TFst(_, ty) => *ty,
        TypedTerm::TSnd(_, ty) => *ty,
        TypedTerm::TWhen(_, ty) => *ty,
        TypedTerm::TWhile(_, _, _, ty) => *ty,
        TypedTerm::TDiff(_, _, _, _, ty) => *ty,
    }
}

pub type BTerm = BTermI<Term>;
pub type TypedBTerm = BTermI<TypedTerm>;
pub type ConcreteTypedBTerm = BTermI<ConcreteTypedTerm>;

#[derive(Clone, Debug, PartialEq)]
pub enum BTermI<A> {
    BTrue,
    BFalse,
    LT(Box<A>,Box<A>),
    Or(Box<BTermI<A>>,Box<BTermI<A>>),
    And(Box<BTermI<A>>,Box<BTermI<A>>)
}

#[derive(Clone, Debug, PartialEq)]
pub enum Type {
    R,
    UnitType,
    TVar(String),
    PairType(Box<Type>,Box<Type>)
}

//We use this for post monomorphization.  For code lowering, 
//monomorphize the code through the main method.  Or in the JIT environment,
//The code is lowered to a simplified form if it has generics in it, and then 
//on an eval, it is monomorphized, lowered, and then evaluated.
//I like correct by construction code, so I like to have the datatypes 
//structured so that transformations move from one datatype to the other 
//and in doing so express certain properties -- for example, the absence of derivatives
//and type variables.
#[derive(Clone,Debug,PartialEq)]
pub enum ConcreteType {
    CR,
    CUnitType,
    CPairType(Box<ConcreteType>,Box<ConcreteType>)
}


/*
An example concrete typed program just for testing purposes
*/
pub fn egConcTypedProg () -> ConcreteTypedProgram{
    let mut prog = Vec::new();
    let my_fun = egConcTypedFun();
    prog.push(my_fun);
    prog
}

pub fn egConcTypedFun() -> ConcreteTypedFunDef {
    let my_fun_body = egConcTypedTerm();
    let args = vec![("x".to_string(),Box::new(ConcreteType::CR)),("y".to_string(),Box::new(ConcreteType::CR)),("z".to_string(),Box::new(ConcreteType::CR))];
    let ret_ty = Box::new(ConcreteType::CR);
    ConcreteTypedFunDef::ConcreteTypedFunDef("f".to_string(),args,ret_ty,Box::new(my_fun_body))
}

pub fn egConcTypedTerm() -> ConcreteTypedTerm {
    //"x+y+z"
    let xvar = ConcreteTypedTerm::TVar("x".to_string(),Box::new(ConcreteType::CR));
    let yvar = ConcreteTypedTerm::TVar("y".to_string(),Box::new(ConcreteType::CR));
    let zvar = ConcreteTypedTerm::TVar("z".to_string(),Box::new(ConcreteType::CR));
    let xplusy = ConcreteTypedTerm::TPlus(Box::new(xvar),Box::new(yvar),Box::new(ConcreteType::CR));
    let xyplusz = ConcreteTypedTerm::TPlus(Box::new(xplusy),Box::new(zvar),Box::new(ConcreteType::CR));
    xyplusz
}

// ConcreteTypedFunDef(String,Vec<(String,Box<ConcreteType>)>,Box<ConcreteType>,Box<ConcreteTypedTerm>)
// TNumber(f64),
// TVar(String,Box<Typ>), //x:A
// TFunCall(String,Box<Typ>,Vec<Box<TypedTermI<Typ>>>),//f:A(args)
// TLet(String,Box<Typ>,Box<TypedTermI<Typ>>,Box<TypedTermI<Typ>>,Box<Typ>),//x:A = m in n:B
// TZero(Box<Typ>),//0:A
// TUnit,
// TPlus(Box<TypedTermI<Typ>>,Box<TypedTermI<Typ>>,Box<Typ>),//m+n:A
// TPair(Box<TypedTermI<Typ>>,Box<TypedTermI<Typ>>,Box<Typ>),//(m,n):X -- by type checking X===(Y,Z)